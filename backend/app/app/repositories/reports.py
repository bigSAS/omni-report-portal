from app.database.models.report import Report as ReportEntity
from app.repositories import Repository


class ReportsRepository(Repository):
    """
    Reports repository.
    """
    entity = ReportEntity
