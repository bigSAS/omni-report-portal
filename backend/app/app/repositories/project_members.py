from app.database.models.project_member import ProjectMember as ProjectMemberEntity
from app.repositories import Repository


class ProjectMembersRepository(Repository):
    """
    Permissions repository.
    """
    entity = ProjectMemberEntity
