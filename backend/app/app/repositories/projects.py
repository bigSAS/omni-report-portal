from app.database.models.project import Project as ProjectEntity
from app.repositories import Repository


class ProjectsRepository(Repository):
    """
    Projects repository.
    """
    entity = ProjectEntity
