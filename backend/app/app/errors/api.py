from app.errors import AppError, ErrorMessage


class ApiError(AppError):
    """
    General api error. Inherit from it as much as u wish.
    Can be called with one message as str (without context)
    Can be called with many messages as list[dict] (with context)

    Examples:
        ApiError('foo')
        ApiError([{'message': 'foo', 'context': 'ctx'}])
    """
    status_code = 500

    def __init__(self, *args):
        edata = args[0]
        if isinstance(edata, str):
            messages = [ErrorMessage(message=edata)]
        elif isinstance(edata, list):
            messages = [ErrorMessage(message=e['message'], context=e['context']) for e in edata]
        elif isinstance(edata, tuple):
            messages = [ErrorMessage(message=edata[0], context=edata[1])]
        else:
            raise NotImplementedError(f'Ivalid data type {type(edata)}')
        super().__init__(messages)

    def __repr__(self):
        return f'[{self.status_code}] {super().__repr__()}'


class AuthError(ApiError):
    """
    Authentication error [401]
    """
    status_code = 401


class ForbiddenError(ApiError):
    """
    Api resource forbidden error [403]
    """
    status_code = 403


class BadRequestError(ApiError):
    """
    Api request invalid data error [400]
    """
    status_code = 400


class NotFoundError(ApiError):
    """
    Api not found error [404]
    """
    status_code = 404


class UnknownApiError(ApiError):
    """
    Api unknown error (bug) [500]
    """
    status_code = 500

# todo: add missing error classes if necessary
