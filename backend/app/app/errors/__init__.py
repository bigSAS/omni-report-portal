from typing import List, Optional

from pydantic import BaseModel


class ErrorMessage(BaseModel):
    message: str
    context: Optional[str]


class AppError(Exception):
    """
    Base app error class. Use for inheritance.
    """

    def __init__(self, messages: List[ErrorMessage]):
        self._messages = messages

    @property
    def messages(self):
        return self._messages

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return f'[{self.__class__.__name__}] {self.messages}'
