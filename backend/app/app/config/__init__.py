import os
from pydantic import BaseSettings


class Settings(BaseSettings):
    APP_NAME: str = "Omni Report Portal"
    EMAIL_SENDER: str = "no-reply@app.com"
    SMTP_SERVER: str = "your_stmp_server_here"

    POSTGRES_USER: str = os.environ.get("POSTGRES_USER", "app")
    POSTGRES_PASSWORD: str = os.environ.get("POSTGRES_PASSWORD", "app")
    POSTGRES_SERVER: str = os.environ.get("POSTGRES_SERVER", "db")
    POSTGRES_DB: str = os.environ.get("POSTGRES_DB", "app")


settings = Settings()
