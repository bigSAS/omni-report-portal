import uuid
from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.dialects.postgresql import JSONB, UUID

from app.database.models.types import AwareDateTime
from app.database.setup import Base
from datetime import datetime


class Report(Base):
    """
    Report entity. Import as ReportEntity.
    """
    __tablename__ = "reports"

    id = Column(UUID(as_uuid=True), primary_key=True, index=True, default=uuid.uuid4)
    project_id = Column(Integer, ForeignKey("projects.id", ondelete='CASCADE'), nullable=False)
    created_date = Column(AwareDateTime, default=datetime.utcnow, nullable=False)
    meta = Column(JSONB, nullable=False)
    data = Column(JSONB, nullable=False)
    summary = Column(JSONB, nullable=False)


def get_summary(meta: dict, data: dict) -> dict:
    summary_data = {
        'total_features': 0,
        'total_scenarios': 0,
        'total_steps': 0,
        'passed_features': 0,
        'passed_scenarios': 0,
        'failed_features': 0,
        'failed_scenarios': 0,
        'passed_steps': 0,
        'failed_steps': 0,
        'skipped_steps': 0
    }

    def scenario_passed(scenario_obj):
        for s in scenario_obj['steps']:
            if s['result']['status'] != 'passed':
                return False
        return True

    if meta['type'] == 'JAVA_CUCUMBER_JSON':  # todo: other types when implemented
        for feature in data:
            summary_data['total_features'] += 1
            before = summary_data['failed_scenarios']
            for scenario in feature['elements']:
                if scenario.get('type', 'unknown') == 'scenario':
                    summary_data['total_scenarios'] += 1
                    if scenario_passed(scenario):
                        summary_data['passed_scenarios'] += 1
                    else:
                        summary_data['failed_scenarios'] += 1
                    for step in scenario['steps']:
                        summary_data['total_steps'] += 1
                        if step['result']['status'] == 'failed':
                            summary_data['failed_steps'] += 1
                        elif step['result']['status'] == 'skipped':
                            summary_data['skipped_steps'] += 1
                        else:
                            summary_data['passed_steps'] += 1
            if summary_data['failed_scenarios'] > before:
                summary_data['failed_features'] += 1
            else:
                summary_data['passed_features'] += 1
    summary_data['passed'] = meta['ci_job_status'] == 'PASS'
    return summary_data
