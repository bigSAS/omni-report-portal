from datetime import datetime
from sqlalchemy import Column, Integer, ForeignKey, String, Boolean
from app.database.models.types import AwareDateTime
from app.database.setup import Base


class Project(Base):
    """
    Project entity. Import as ProjectEntity.

    {
        "name": "My cool project",
        "slug": "MQP-SAS",
        "description": null
    }
    """
    __tablename__ = "projects"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False)
    slug = Column(String, unique=True, index=True, nullable=False)
    description = Column(String)
    created_date = Column(AwareDateTime, default=datetime.now, nullable=False)
    is_private = Column(Boolean, nullable=False, default=True)
    created_by_user_id = Column(Integer, ForeignKey("users.id", ondelete='CASCADE'), nullable=False)
