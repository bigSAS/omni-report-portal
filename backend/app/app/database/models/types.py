import pytz  # from PyPI
from sqlalchemy import TypeDecorator, DateTime


class AwareDateTime(TypeDecorator):
    """
    Results returned as aware datetimes, not naive ones.
    """

    impl = DateTime

    def process_result_value(self, value, dialect):
        return value.replace(tzinfo=pytz.utc)
