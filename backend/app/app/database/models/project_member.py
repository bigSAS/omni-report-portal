from sqlalchemy import Column, Integer, ForeignKey
from app.database.setup import Base


class ProjectMember(Base):
    """
    ProjectMember entity. Import as ProjectMemberEntity.
    """
    __tablename__ = "project_members"

    id = Column(Integer, primary_key=True, index=True)
    project_id = Column(Integer, ForeignKey("projects.id", ondelete='CASCADE'), nullable=False)
    user_id = Column(Integer, ForeignKey("users.id", ondelete='CASCADE'), nullable=False)
