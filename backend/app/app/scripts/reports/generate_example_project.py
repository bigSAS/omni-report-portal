import json
import uuid

from sqlalchemy import text
from app.api.schemas.permission import ProjectPermissions
from app.api.schemas.report import ReportMeta, ReportTypes
from app.database.models.permission import Permission
from app.database.models.project import Project
from app.database.models.project_member import ProjectMember
from app.database.models.report import Report, get_summary
from app.database.setup import SessionLocal
from app.repositories.permissions import PermissionsRepository
from app.repositories.project_members import ProjectMembersRepository
from app.repositories.projects import ProjectsRepository
from app.repositories.reports import ReportsRepository
from app.repositories.users import UserRepository


def init_example_test_project():
    db = SessionLocal()
    user_repository = UserRepository(db)
    admin = user_repository.get_by(username='admin', ignore_not_found=True)
    project_repository = ProjectsRepository(db)
    reports_repository = ReportsRepository(db)
    reports = [
        {
            'file': 'JAVA_CUCUMBER_JSON.example.passed.json',
            'meta': ReportMeta(
                type=ReportTypes.JAVA_CUCUMBER_JSON,
                title="JAVA_CUCUMBER_JSON.example",
                tags=["java", "cucumber", "example"],
                info="...",
                ci_system="jenkins",
                ci_job_url="https://ils-jenkins.easypack24.net/job/tms--tests/job/pipelines/job/pierony/job/API/job/"
                           "TEST/job/INTEGRATIONS/116/",
                ci_job_status="PASS"
            )
        },
        {
            'file': 'JAVA_CUCUMBER_JSON.api.failed.json',
            'meta': ReportMeta(
                type=ReportTypes.JAVA_CUCUMBER_JSON,
                title="JAVA_CUCUMBER_JSON api with failed scenarios",
                tags=["java", "cucumber", "api", "with-failed"],
                info="...",
                ci_system="jenkins",
                ci_job_url="https://ils-jenkins.easypack24.net/job/tms--tests/job/pipelines/job/pierony/job/API/job/TEST/job/INTEGRATIONS/142/",
                ci_job_status="FAIL"
            )
        },
        {
            'file': 'JAVA_CUCUMBER_JSON.gui.failed.with.screenshots.json',
            'meta': ReportMeta(
                type=ReportTypes.JAVA_CUCUMBER_JSON,
                title="JAVA_CUCUMBER_JSON gui with failed scenarios with screenshots",
                tags=["java", "cucumber", "gui", "with-failed", "screenshots"],
                info="...",
                ci_system="jenkins",
                ci_job_url="https://ils-jenkins.easypack24.net/job/tms--tests/job/pipelines/job/pierony/job/WebUI/job/TEST/job/CourierServiceGroupB/118/",
                ci_job_status="FAIL"
            )
        },
        # todo: update when other types are handled
    ]
    slug = "TEST_EX"
    project = Project(
        name="Example OPEN TEST project",
        slug=slug,
        created_by_user_id=admin.id,
        is_private=False
    )
    existing = project_repository.get_by(slug=slug, ignore_not_found=True)
    if existing:
        project_repository.delete_entity(existing, autocommit=False)
        # remove permissions
        permissions_repository = PermissionsRepository(db)
        permissions_repository.delete_by_filter(
            text(f"CAST(data->>'project_slug' AS VARCHAR) = '{slug}'"), autocommit=False)
        permissions_repository.commit()
    project_repository.save(project)

    owner_member = ProjectMember(user_id=admin.id, project_id=project.id)
    ProjectMembersRepository(db).save(owner_member)
    # add project permissions
    permissions = [ProjectPermissions.IS_PROJECT_ADMIN, ProjectPermissions.CAN_CREATE_REPORT]
    permission_repository = PermissionsRepository(db)
    for permission in permissions:
        project_admin_permission = Permission(
            name=permission,
            data={'project_slug': project.slug},
            user_id=admin.id
        )
        permission_repository.save(project_admin_permission, autocommit=False)
    permission_repository.commit()

    for i in range(30):
        for report in reports:
            file = report['file']
            with open(f'app/scripts/reports/{file}', mode='r', encoding='UTF-8') as f:
                report_data = json.load(f)
                meta = report['meta'].dict()
                meta['title'] = meta['title'] + ' ' + str(uuid.uuid4())[:7]
                new_report = Report(
                    project_id=project.id,
                    meta=meta,
                    data=report_data,
                    summary=get_summary(meta, report_data)
                )
                reports_repository.save(new_report, autocommit=False)
        reports_repository.commit()


if __name__ == '__main__': init_example_test_project()
