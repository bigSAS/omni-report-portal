from app.database.models.project import Project
from app.database.models.report import Report, get_summary
from app.database.setup import SessionLocal
from app.repositories.reports import ReportsRepository


def update_summarys():
    db = SessionLocal()
    repo = ReportsRepository(db)
    all_reports = repo.all()
    for report in all_reports:
        print('before', report.summary)
        print('before', report.project_id)
        new_summary = get_summary(report.meta, report.data)
        report.summary = new_summary
        print('after', report.summary)
        print('commit')
        db.commit()
        # print(report)

    print('done.')


if __name__ == '__main__': update_summarys()
