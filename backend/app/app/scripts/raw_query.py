from app.database.setup import SessionLocal
# 4 debugin custom raw sqls
db = SessionLocal()
q = """
SELECT 
    COUNT(id), 
    SUM(CAST(summary->>'total_features' AS INTEGER)),
    SUM(CAST(summary->>'total_scenarios' AS INTEGER)),
    SUM(CAST(summary->>'passed_features' AS INTEGER)),
    SUM(CAST(summary->>'passed_scenarios' AS INTEGER)),
    SUM(CAST(summary->>'failed_features' AS INTEGER)),
    SUM(CAST(summary->>'failed_scenarios' AS INTEGER)),
    SUM(CAST(summary->>'passed_steps' AS INTEGER)),
    SUM(CAST(summary->>'failed_steps' AS INTEGER))
FROM reports
;
"""
rows = db.execute(q)
for row in rows:
    print(row)
# WHERE project_id = 'TEST_EX';