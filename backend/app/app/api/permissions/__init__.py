from abc import ABC, abstractmethod

from app.api.schemas.token import TokenData


class BasePermission(ABC):
    """
    Permission abstraction.
    Child class should implement has_permission method.
    check_permission should always be called with kwargs.
    """
    def __init__(self, **kwargs):
        self.db = kwargs.get('db', None)
        if not self.db: raise ValueError('db not passed to Permission instance!')
        self.token: TokenData = kwargs.get('token_data', None)

    @abstractmethod
    def check_permission(self, **kwargs):
        """
        Should raise ForbiddenError when not satisfied.
        """
        pass
