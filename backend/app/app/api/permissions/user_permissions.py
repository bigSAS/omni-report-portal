from typing import Optional, List, Union

from starlette.requests import Request

from app.api.permissions import BasePermission
from app.api.schemas.permission import Permissions, ProjectPermissions
from app.api.schemas.token import TokenData
from app.errors.api import ForbiddenError, NotFoundError
from app.errors.repository import EntityNotFoundError
from app.repositories.projects import ProjectsRepository


def _get_project(slug: str, db):
    try:
        return ProjectsRepository(db).get_by(slug=slug.upper())
    except EntityNotFoundError as e:
        raise NotFoundError(repr(e))


def _get_project_slug(url) -> Optional[str]:
    splitted = url.path.split("/")
    try:
        project_idx = splitted.index('projects')
    except ValueError:
        return None
    slug_idx = project_idx + 1
    try:
        return splitted[slug_idx].upper()
    except IndexError:
        return None


def _is_admin(token: Union[TokenData, None]) -> bool:
    if token is None: return False
    return len([p for p in token.permissions if p.get('name', None) == Permissions.IS_ADMIN]) > 0


def _filter_permissions_by_name(permisison: Union[Permissions, ProjectPermissions], token: TokenData) -> List[dict]:
    if token is None: return []
    return list(filter(lambda p: p['name'] == permisison, token.permissions))


def _filter_permission_data(permissions: List[dict], data_attr: str, data_default: str = '') -> List[str]:
    return [p['data'].get(data_attr, data_default) for p in permissions]

# todo: tu mozna by pokminic lepiej zrobic permissiony (pod opened project) narazie chyba wsio dziala
#  - mozna przekminnic innym razem


class IsAdmin(BasePermission):
    """
    Check if JWT user is sys admin.
    """
    def check_permission(self, **kwargs):
        if not _is_admin(self.token):
            raise ForbiddenError("Admins only ... sorry :)")


class IsProjectCreator(BasePermission):
    """
    Check if JWT user is project creator.
    """
    def check_permission(self, **kwargs):
        request: Request = kwargs.get("request")
        slug = _get_project_slug(request.url)
        if not slug: raise ValueError(f'IsProjectCreator not found project slug in url: {request.url.path}')
        if _is_admin(self.token): return True
        if slug not in self.token.user_owned_projects:
            raise ForbiddenError(f"You're not the owner of project \"{slug}\" sorry.")


class IsProjectAdmin(BasePermission):
    """
    Check if JWT user is project admin.
    """
    def check_permission(self, **kwargs):
        request: Request = kwargs.get("request")
        project_slug = _get_project_slug(request.url)
        if not project_slug: raise ValueError(f'IsProjectCreator not found project slug in url: {request.url.path}')
        if _is_admin(self.token): return True
        slug = project_slug.upper()
        permissions = _filter_permissions_by_name(ProjectPermissions.IS_PROJECT_ADMIN, self.token)
        if slug not in _filter_permission_data(permissions, 'project_slug'):
            raise ForbiddenError(f"You're not admin in project \"{project_slug}\" sorry.")


class IsProjectMember(BasePermission):
    """
    Check if JWT user is project member or sys admin.
    """

    def check_permission(self, **kwargs):
        request: Request = kwargs.get("request")
        slug = _get_project_slug(request.url)
        if not slug: raise ValueError(f'IsProjectMember not found project slug in url: {request.url.path}')
        project = _get_project(slug, self.db)
        if _is_admin(self.token): return True
        if not project.is_private: return True
        if self.token is None or slug not in self.token.user_member_of_projects:
            raise ForbiddenError(f"You're not member of project \"{slug}\" sorry.")


class CanCreateReport(BasePermission):
    """
    Check if JWT user is project member or sys admin and has permission to create report
    """
    def check_permission(self, **kwargs):
        request: Request = kwargs.get("request")
        slug = _get_project_slug(request.url)
        if not slug: raise ValueError(f'CanCreateReport not found project slug in url: {request.url.path}')
        if _is_admin(self.token): return True
        if self.token is None or slug not in self.token.user_member_of_projects:
            raise ForbiddenError(f"You're not member of project \"{slug}\" sorry.")
        permissions = _filter_permissions_by_name(ProjectPermissions.CAN_CREATE_REPORT, self.token)
        if slug not in _filter_permission_data(permissions, 'project_slug'):
            raise ForbiddenError(f"You're not allowed to create report in project \"{slug}\" sorry.")
