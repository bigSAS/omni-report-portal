from pydantic import BaseModel


class ProjectMemberBase(BaseModel):
    user_id: int


# noinspection PyMethodParameters
class ProjectMemberCreate(ProjectMemberBase): pass


class ProjectMember(ProjectMemberBase):
    id: int
    project_id: int
    class Config: orm_mode = True
