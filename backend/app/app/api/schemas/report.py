from enum import Enum

from pydantic import BaseModel, UUID4, validator, constr
from typing import List, Optional, Union
from datetime import datetime
from app.api.schemas.pagination import PaginatedModel


class ReportBase(BaseModel):
    meta: dict


class ReportTypes(str, Enum):
    JAVA_CUCUMBER_JSON = "JAVA_CUCUMBER_JSON"


class ReportJobStatus(str, Enum):
    PASS = "PASS"
    FAIL = "FAIL"
    UNKNOWN = "UNKNOWN"


class ReportMeta(BaseModel):
    """
    example meta:
        {
            "type": "JAVA_CUCUMBER_JSON",
            "tags": ["foo", "barz"],
            "title": "some report title",
            "info": "some short info",
            "ci_system": "jenkins",
            "ci_job_url": "https://foo.bar/99",
            "ci_job_status": "PASS",
        }
    """
    type: ReportTypes
    tags: List[constr(min_length=2, max_length=20)]
    title: constr(min_length=3, max_length=100)
    info: Optional[constr(max_length=500)]
    ci_system: Optional[constr(min_length=2, max_length=50)]
    ci_job_url: Optional[constr(min_length=6, max_length=2000)]
    ci_job_status: Optional[ReportJobStatus]


# noinspection PyMethodParameters,PyUnusedLocal
class ReportCreate(ReportBase):
    meta: ReportMeta
    data: Union[dict, List[dict]]

    @validator('meta')
    def validate_meta(cls, meta: ReportMeta) -> ReportMeta:
        return meta

    @validator('data')
    def validate_data(cls, data, values: dict, **kwargs) -> dict:
        meta: ReportMeta = values['meta']
        if meta.type == ReportTypes.JAVA_CUCUMBER_JSON:
            if not isinstance(data, list):
                raise ValueError('Report data should be a list of objects.')
            required_attrs = ['name', 'tags', 'keyword', 'elements']
            for item in data:
                for required_attr in required_attrs:
                    if item.get(required_attr, None) is None:
                        raise ValueError(f'Report item is missing attr: {required_attr}\n {item}')
        return data


class Report(ReportBase):
    id: UUID4
    project_id: int
    created_date: datetime
    data: Union[dict, List[dict]]
    summary: dict
    class Config: orm_mode = True


class ReportMinimal(ReportBase):
    id: UUID4
    project_id: int
    created_date: datetime
    class Config: orm_mode = True


class ReportsPaginated(PaginatedModel):
    """
    ReportMinimal (without data) list with pagination.
    """
    items: List[ReportMinimal]
    class Meta: orm_model_class = ReportMinimal

# todo: ReportsStatistics -> based on query return statistics (summary: total ect.)
