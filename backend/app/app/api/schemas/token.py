from pydantic import BaseModel
from typing import List


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str
    email: str
    permissions: List[dict]
    user_groups: List[dict]
    user_owned_projects: List[str]
    user_member_of_projects: List[str]
