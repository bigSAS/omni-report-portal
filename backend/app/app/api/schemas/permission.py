from pydantic import BaseModel
from typing import Optional, List, Union

from app.api.schemas.pagination import PaginatedModel
from enum import Enum


class ProjectPermissions(str, Enum):
    CAN_CREATE_REPORT = "CAN_CREATE_REPORT"  # has data {project_slug: FOO}
    IS_PROJECT_ADMIN = "IS_PROJECT_ADMIN"  # has data {project_slug: FOO}


class Permissions(str, Enum):
    IS_ADMIN = "IS_ADMIN"
    IS_GROUP_MEMBER = "IS_GROUP_MEMBER"
    HAS_ACTION_ACCESS = "HAS_ACTION_ACCESS"


class PermissionBase(BaseModel):
    name: Union[Permissions, ProjectPermissions]
    data: Optional[dict] = None


class PermissionCreate(PermissionBase):
    user_id: int

    # todo: validate permission name
    # todo: validate permission data
    # ^ 1 custom validation ? pydantic docs -> l8r


class ProjectPermissionCreate(BaseModel):
    name: ProjectPermissions
    user_id: int


class ProjectPermissionDelete(ProjectPermissionCreate):
    pass


class ProjectPermission(PermissionBase):
    id: int
    user_id: int
    name: ProjectPermissions
    class Config: orm_mode = True


class Permission(PermissionBase):
    id: int
    user_id: int
    class Config: orm_mode = True


class PermissionsPaginated(PaginatedModel):
    """
    Note list with pagination.
    """
    items: List[Permission]
    class Meta: orm_model_class = Permission


class ProjectPermissionsPaginated(PaginatedModel):
    """
    Note list with pagination.
    """
    items: List[ProjectPermission]
    class Meta: orm_model_class = ProjectPermission
