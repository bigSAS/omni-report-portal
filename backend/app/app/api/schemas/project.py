from re import match, search
from pydantic import BaseModel, constr, validator
from typing import List, Optional
from datetime import datetime
from app.api.schemas.pagination import PaginatedModel


# noinspection PyMethodParameters
class ProjectCreate(BaseModel):
    name: str
    slug: constr(min_length=3, max_length=10)
    description: Optional[constr(max_length=500)]
    is_private: bool

    @validator('slug')
    def syntax_check(cls, slug):
        if search(r'\s', slug): raise ValueError('Ivalid format. No white space allowed')
        if not match(r'([A-Za-z_]+)', slug): raise ValueError('Ivalid format. Only letters and low dashes accepted')
        if '-' in slug: raise ValueError('Ivalid format. Only letters and low dashes accepted')
        if not match(r'^[^_].*[^_]$', slug): raise ValueError('Ivalid format. Cannot start or end with low dash')
        return slug.upper()


class Project(BaseModel):
    id: int
    name: str
    slug: str
    description: Optional[str]
    created_date: datetime
    is_private: bool
    created_by_user_id: int
    class Config: orm_mode = True

    # @validator('created_date')
    # def name_must_contain_space(cls, value):
    #     return value + timedelta(hours=2)


class ProjectSummary(BaseModel):
    total_reports: int
    passed_reports: int
    failed_reports: int
    total_features: int
    total_scenarios: int
    passed_features: int
    passed_scenarios: int
    failed_features: int
    failed_scenarios: int


class ProjectsPaginated(PaginatedModel):
    """
    Project list with pagination.
    """
    items: List[Project]
    class Meta: orm_model_class = Project
