from re import search, match
from typing import List, Optional

from pydantic import BaseModel, constr, validator
from app.api.schemas.pagination import PaginatedModel


class UserBase(BaseModel):
    username: str
    email: str


# noinspection PyMethodParameters
class UserCreate(UserBase):
    """
    Create (Register) new user.
    """
    username: constr(min_length=3, max_length=20)
    password: constr(min_length=8, max_length=30)

    @validator('password')
    def must_obey_password_rules(cls, password: str):
        if search(r'\s', password): raise ValueError('Ivalid format. No white space allowed')
        return password

    @validator('username')
    def must_obey_username_rules(cls, username: str):
        if search(r'\s', username): raise ValueError('Ivalid format. No white space allowed')
        if not match(r'([A-Za-z_]+)', username): raise ValueError('Ivalid format. Only letters and low dashes accepted')
        if '-' in username: raise ValueError('Ivalid format. Only letters and low dashes accepted')
        return username


class UserPermission(BaseModel):
    name: str
    data: Optional[dict]


class UserGroup(BaseModel):
    group_id: int


class UserMe(UserBase):
    id: int
    permissions: List[UserPermission]
    user_groups: List[UserGroup]


class User(UserBase):
    id: int
    is_active: bool
    is_deleted: bool
    class Config: orm_mode = True


class ChangePassword(BaseModel):
    email: str
    old_password: str
    new_password: str
    new_password_repeat: str


class UsersPaginated(PaginatedModel):
    """
    User list with pagination.
    """
    items: List[User]
    class Meta: orm_model_class = User
