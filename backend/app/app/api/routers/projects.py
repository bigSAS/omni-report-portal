from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from sqlalchemy import text
from pydantic import UUID4
from app.api.permissions.user_permissions import IsProjectCreator, IsProjectMember, CanCreateReport, IsProjectAdmin
from app.api.routers import responses as res
from app.api.dependencies import get_db, authenticated_user, Restricted
from app.api.schemas.permission import ProjectPermissionCreate, ProjectPermissionDelete, \
    ProjectPermission, \
    ProjectPermissionsPaginated, ProjectPermissions
from app.api.schemas.project import ProjectsPaginated, Project, ProjectCreate, ProjectSummary
from app.api.schemas.project_member import ProjectMember, ProjectMemberCreate
from app.api.schemas.report import ReportsPaginated, ReportCreate, Report
from app.api.schemas.user import UsersPaginated
from app.database.models.permission import Permission as PermissionEntity
from app.errors.api import BadRequestError, NotFoundError
from app.errors.repository import EntityNotFoundError
from app.repositories.permissions import PermissionsRepository
from app.repositories.project_members import ProjectMembersRepository
from app.repositories.projects import ProjectsRepository
from app.database.models.project import Project as ProjectEntity
from app.database.models.user import User as UserEntity
from app.repositories.reports import ReportsRepository
from app.repositories.users import UserRepository
from app.database.models.project_member import ProjectMember as ProjectMemberEntity
from app.database.models.report import Report as ReportEntity, get_summary

router = APIRouter()


@router.get("/projects", tags=['projects'],
            response_model=ProjectsPaginated,
            responses=res.AUTHENTICATED,
            )  # dependencies=[Depends(authenticated_user)]
async def list_projects(
        page: int = 0, limit: int = 100, order_by: str = None,
        db: Session = Depends(get_db)):
    """
    List projects paginated.
    """
    # todo: if query
    return ProjectsPaginated.from_paginated_query(
        ProjectsRepository(db).all_paginated(page=page, limit=limit, order=order_by)
    )


@router.get("/projects/mine", tags=['projects'],
            response_model=ProjectsPaginated,
            responses=res.AUTHENTICATED,
            dependencies=[Depends(authenticated_user)])
async def list_mine_projects(
        page: int = 0, limit: int = 100, order_by: str = None,
        db: Session = Depends(get_db), user: UserEntity = Depends(authenticated_user)):
    """
    List projects of authenticated user (owner, member).
    """
    member_projects = ProjectMembersRepository(db).filter(ProjectMemberEntity.user_id == user.id)
    member_project_ids = [p.project_id for p in member_projects]
    owner_projects = ProjectsRepository(db).filter(ProjectEntity.created_by_user_id == user.id)
    owner_project_ids = [p.id for p in owner_projects]
    ids = set(member_project_ids + owner_project_ids)
    return ProjectsPaginated.from_paginated_query(
        ProjectsRepository(db).filter_paginated(ProjectEntity.id.in_(ids), page=page, limit=limit, order=order_by)
    )


@router.post("/projects", tags=['projects'],
             response_model=Project,
             responses=res.AUTHENTICATED)
async def create_project(
        form_data: ProjectCreate,
        db: Session = Depends(get_db),
        user: UserEntity = Depends(authenticated_user)):
    """
    Create project.
    """
    project_repository = ProjectsRepository(db)
    slug = form_data.slug.upper()
    # add project
    new_project = ProjectEntity(
        name=form_data.name,
        description=form_data.description,
        slug=slug,
        created_by_user_id=user.id,
        is_private=form_data.is_private
    )
    existing = project_repository.get_by(slug=slug, ignore_not_found=True)
    if existing: raise BadRequestError((f'Project with slug "{slug}" already exists', 'body.slug'))
    project_repository.save(new_project)
    # add owner (createor as member)
    owner_member = ProjectMemberEntity(user_id=user.id, project_id=new_project.id)
    ProjectMembersRepository(db).save(owner_member)
    # add project permissions
    permissions = [ProjectPermissions.IS_PROJECT_ADMIN, ProjectPermissions.CAN_CREATE_REPORT]
    permission_repository = PermissionsRepository(db)
    for permission in permissions:
        project_admin_permission = PermissionEntity(
            name=permission,
            data={'project_slug': new_project.slug},
            user_id=user.id
        )
        permission_repository.save(project_admin_permission, autocommit=False)
    permission_repository.commit()
    return new_project


@router.get("/projects/{slug}", tags=['projects'],
            response_model=Project,
            responses=res.AUTHENTICATED | res.PROTECTED | res.NOT_FOUND,
            dependencies=[
                # Depends(authenticated_user),
                Depends(Restricted([IsProjectMember]))
            ])
async def get_project(slug: str, db: Session = Depends(get_db)):
    """
    Get project. Only for project members / owners.
    """
    try:
        return ProjectsRepository(db).get_by(slug=slug.upper())
    except EntityNotFoundError as e:
        raise NotFoundError(repr(e))


@router.get("/projects/{slug}/summary", tags=['projects'],
            response_model=ProjectSummary,
            responses=res.AUTHENTICATED | res.PROTECTED | res.NOT_FOUND,
            dependencies=[
                # Depends(authenticated_user),
                Depends(Restricted([IsProjectMember]))
            ])
async def get_project_summary(slug: str, db: Session = Depends(get_db)):
    """
    Get project summary. Only for project members / owners.
    """
    try:
        project = ProjectsRepository(db).get_by(slug=slug.upper())
    except EntityNotFoundError as e:
        raise NotFoundError(repr(e))
    summary_data = {
        'total_reports': 0,
        'failed_reports': 0,
        'passed_reports': 0,
        'total_features': 0,
        'total_scenarios': 0,
        'passed_features': 0,
        'passed_scenarios': 0,
        'failed_features': 0,
        'failed_scenarios': 0,
    }
    # liczenie po stronie sql
    q = f"""
    SELECT 
        COUNT(id),
        SUM(CAST(summary->>'total_features' AS INTEGER)),
        SUM(CAST(summary->>'total_scenarios' AS INTEGER)),
        SUM(CAST(summary->>'passed_features' AS INTEGER)),
        SUM(CAST(summary->>'passed_scenarios' AS INTEGER)),
        SUM(CAST(summary->>'failed_features' AS INTEGER)),
        SUM(CAST(summary->>'failed_scenarios' AS INTEGER)),
        SUM(CAST(summary->>'passed_steps' AS INTEGER)),
        SUM(CAST(summary->>'failed_steps' AS INTEGER))
    FROM reports
    WHERE project_id = '{project.id}';
    """
    rows = db.execute(q)
    for r in rows:
        summary_data['total_reports'] = r[0] if r[0] else 0
        summary_data['total_features'] = r[1] if r[1] else 0
        summary_data['total_scenarios'] = r[2] if r[2] else 0
        summary_data['passed_features'] = r[3] if r[3] else 0
        summary_data['passed_scenarios'] = r[4] if r[4] else 0
        summary_data['failed_features'] = r[5] if r[5] else 0
        summary_data['failed_scenarios'] = r[6] if r[6] else 0

    q2 = f"""
        SELECT COUNT(id) 
        FROM reports 
        WHERE project_id = '{project.id}' and CAST(summary->>'passed' AS BOOLEAN) = true;
        """
    rows = db.execute(q2)
    for r in rows:
        summary_data['passed_reports'] = r[0] if r[0] else 0
        summary_data['failed_reports'] = summary_data['total_reports'] - summary_data['passed_reports']

    return ProjectSummary(**summary_data)


@router.delete("/projects/{slug}", tags=['projects'],
               status_code=204,
               responses=res.AUTHENTICATED | res.PROTECTED | res.NO_CONTENT | res.NOT_FOUND,
               dependencies=[
                   Depends(authenticated_user),
                   Depends(Restricted([IsProjectCreator]))
               ])
async def delete_project(slug: str, db: Session = Depends(get_db)):
    """
    Delete project. Only for project owners.
    """
    repository = ProjectsRepository(db)
    project_slug = slug.upper()
    try:
        project = repository.get_by(slug=project_slug)
        repository.delete_entity(project, autocommit=False)
        # remove permissions
        permissions_repository = PermissionsRepository(db)
        permissions_repository.delete_by_filter(
            text(f"CAST(data->>'project_slug' AS VARCHAR) = '{project_slug}'"), autocommit=False)
        db.commit()
        return None
    except EntityNotFoundError as e:
        raise NotFoundError(repr(e))


@router.get("/projects/{slug}/members", tags=['project-members'],
            response_model=UsersPaginated,
            responses=res.AUTHENTICATED | res.PROTECTED | res.NOT_FOUND,
            dependencies=[
                # Depends(authenticated_user),
                Depends(Restricted([IsProjectMember]))
            ])
async def list_project_members(slug: str, db: Session = Depends(get_db)):
    """
    List project members paginated.
    """
    try:
        project = ProjectsRepository(db).get_by(slug=slug.upper())
    except EntityNotFoundError as e:
        raise NotFoundError(repr(e))
    project_members = ProjectMembersRepository(db).filter(ProjectMemberEntity.project_id == project.id)
    return UsersPaginated.from_paginated_query(
        UserRepository(db).filter_paginated(UserEntity.id.in_(pm.user_id for pm in project_members))
    )


@router.post("/projects/{slug}/members", tags=['project-members'],
             response_model=ProjectMember,
             responses=res.AUTHENTICATED | res.PROTECTED | res.NOT_FOUND,
             dependencies=[
                 Depends(authenticated_user),
                 Depends(Restricted([IsProjectAdmin]))
             ])
async def add_project_member(
        slug: str,
        form_data: ProjectMemberCreate,
        db: Session = Depends(get_db)):
    """
    Add user to project (ProjectMember)
    """
    project_repository = ProjectsRepository(db)
    user_repository = UserRepository(db)
    project_member_repository = ProjectMembersRepository(db)
    slug = slug.upper()

    try:
        project = project_repository.get_by(slug=slug)
    except EntityNotFoundError as e:
        raise NotFoundError(repr(e))
    try:
        user = user_repository.get(form_data.user_id)
    except EntityNotFoundError as e:
        raise BadRequestError((repr(e), "body.user_id"))
    new_member = ProjectMemberEntity(
        project_id=project.id,
        user_id=user.id
    )
    existing_member = project_member_repository.get_by(project_id=project.id, user_id=user.id, ignore_not_found=True)
    if existing_member:
        raise BadRequestError((
            f'user "{user.username}" is already a member of project "{project.slug}"',
            "body.user_id"
        ))
    project_member_repository.save(new_member)
    return new_member


@router.delete("/projects/{slug}/members", tags=['project-members'],
               status_code=204,
               responses=res.AUTHENTICATED | res.PROTECTED | res.NOT_FOUND | res.NO_CONTENT,
               dependencies=[
                   Depends(authenticated_user),
                   Depends(Restricted([IsProjectAdmin]))
               ])
async def remove_project_member(
        slug: str,
        form_data: ProjectMemberCreate,
        db: Session = Depends(get_db)):
    """
    Remove user from project (ProjectMember)
    """
    project_repository = ProjectsRepository(db)
    user_repository = UserRepository(db)
    project_member_repository = ProjectMembersRepository(db)
    slug = slug.upper()

    try:
        project = project_repository.get_by(slug=slug)
    except EntityNotFoundError as e:
        raise NotFoundError(repr(e))
    try:
        user = user_repository.get(form_data.user_id)
    except EntityNotFoundError as e:
        raise BadRequestError((repr(e), "body.user_id"))
    existing_member = project_member_repository.get_by(project_id=project.id, user_id=user.id, ignore_not_found=True)
    if not existing_member:
        raise BadRequestError((
            f'user "{user.username}" is not a member of project "{project.slug}"',
            "body.user_id"
        ))
    project_member_repository.delete_entity(existing_member)
    return None


@router.get("/projects/{slug}/reports", tags=['project-reports'],
            response_model=ReportsPaginated,
            responses=res.AUTHENTICATED | res.PROTECTED | res.NOT_FOUND,
            dependencies=[
                # Depends(authenticated_user),
                Depends(Restricted([IsProjectMember]))])
async def list_reports(
        slug: str, page: int = 0, limit: int = 100, order_by: str = None, query: str = None,
        db: Session = Depends(get_db)):
    """
    List reports for projec paginated.
    """
    # !!! start todo: experimental query filtering -> generalize l8r
    filter_data = {}
    filters = query.split(';') if query else []
    for f in filters:
        fsplit = f.split('=')
        fname = fsplit[0]
        fvalue = fsplit[1]
        if fname == 'tags':
            tags = fvalue.split(',')
            print('tags', tags)
            filter_data['tags'] = tags
    # !!! end
    try:
        project = ProjectsRepository(db).get_by(slug=slug.upper())
    except EntityNotFoundError as e:
        raise NotFoundError(repr(e))
    # filterfunc = ReportEntity.project_id == project.id
    q = f"project_id = {project.id} "
    if filter_data.get('tags', None):
        for tag in filter_data['tags']:
            q += f"and meta->'tags' @> '\"{tag}\"' "
    print('@QUERY', q)
    return ReportsPaginated.from_paginated_query(
        ReportsRepository(db).filter_paginated(
            text(q), page=page, limit=limit, order=order_by)
    )


@router.post("/projects/{slug}/reports", tags=['project-reports'],
             response_model=Report,
             responses=res.AUTHENTICATED | res.PROTECTED | res.NOT_FOUND,
             dependencies=[
                 Depends(authenticated_user),
                 Depends(Restricted([CanCreateReport]))])
async def create_report(
        slug: str,
        form_data: ReportCreate,
        db: Session = Depends(get_db)):
    """
    Create report for project.
    """
    try:
        project = ProjectsRepository(db).get_by(slug=slug.upper())
    except EntityNotFoundError as e:
        raise NotFoundError(repr(e))
    summary = get_summary(form_data.meta.dict(), data=form_data.data)
    new_report = ReportEntity(
        meta=form_data.meta.dict(),
        data=form_data.data,
        project_id=project.id,
        summary=summary
    )
    ReportsRepository(db).save(new_report)
    return new_report


@router.get("/projects/{slug}/reports/{report_id}", tags=['project-reports'],
            response_model=Report,
            responses=res.AUTHENTICATED | res.PROTECTED | res.NOT_FOUND,
            dependencies=[
                # Depends(authenticated_user),
                Depends(Restricted([IsProjectMember]))])
async def get_report(
        slug: str,
        report_id: UUID4,
        db: Session = Depends(get_db)):
    """
    Get project report by project slug and report uuid.
    """
    try:
        project = ProjectsRepository(db).get_by(slug=slug.upper())
    except EntityNotFoundError as e:
        raise NotFoundError(repr(e))
    try:
        return ReportsRepository(db).get_by(project_id=project.id, id=report_id)
    except EntityNotFoundError as e:
        raise NotFoundError(repr(e))


@router.delete("/projects/{slug}/reports/{report_id}", tags=['project-reports'],
               status_code=204,
               responses=res.AUTHENTICATED | res.PROTECTED | res.NOT_FOUND,
               dependencies=[
                   Depends(authenticated_user),
                   Depends(Restricted([IsProjectAdmin]))])
async def delete_report(
        slug: str,
        report_id: UUID4,
        db: Session = Depends(get_db)):
    """
    Delete project report by project slug and report uuid.
    """
    try:
        project = ProjectsRepository(db).get_by(slug=slug.upper())
    except EntityNotFoundError as e:
        raise NotFoundError(repr(e))
    try:
        repository = ReportsRepository(db)
        report = repository.get_by(project_id=project.id, id=report_id)
        repository.delete_entity(report)
    except EntityNotFoundError as e:
        raise NotFoundError(repr(e))
    return None


@router.post("/projects/{slug}/permissions", tags=['project-permissions'],
             response_model=ProjectPermission,
             responses=res.AUTHENTICATED | res.PROTECTED | res.NOT_FOUND,
             dependencies=[
                 Depends(authenticated_user),
                 Depends(Restricted([IsProjectAdmin]))])
async def create_project_persmisison(
        slug: str,
        form_data: ProjectPermissionCreate,
        db: Session = Depends(get_db)):
    """
    Create permission for project.
    """
    slug = slug.upper()
    permission_repository = PermissionsRepository(db)
    new_project_permission = PermissionEntity(
        name=form_data.name,
        data={'project_slug': slug},
        user_id=form_data.user_id
    )
    try:
        project = ProjectsRepository(db).get_by(slug=slug.upper())
    except EntityNotFoundError as e:
        raise NotFoundError(repr(e))
    project_member = ProjectMembersRepository(db).get_by(
        project_id=project.id, user_id=new_project_permission.user_id, ignore_not_found=True)
    if not project_member:
        raise BadRequestError((
            f'User: {new_project_permission.user_id} is not a member of project "{slug}"',
            'body.user_id'
        ))
    existing_permissions = permission_repository.filter(text(
        f"user_id = {new_project_permission.user_id} "
        f"and name = '{form_data.name}' "
        f"and CAST(data->>'project_slug' AS VARCHAR) = '{slug}' "
    ))
    if len(existing_permissions) > 0:
        raise BadRequestError((
            f'Permission {new_project_permission.name} already exists for user: {new_project_permission.user_id} '
            f'in project {slug}',
            'body.name'
        ))
    permission_repository.save(new_project_permission)
    return new_project_permission


@router.delete("/projects/{slug}/permissions", tags=['project-permissions'],
               status_code=204,
               responses=res.AUTHENTICATED | res.PROTECTED | res.NOT_FOUND | res.NO_CONTENT,
               dependencies=[
                   Depends(authenticated_user),
                   Depends(Restricted([IsProjectAdmin]))])
async def delete_project_persmisison(
        slug: str,
        form_data: ProjectPermissionDelete,
        db: Session = Depends(get_db)):
    """
    Delete permission for project.
    """
    slug = slug.upper()
    try:
        project = ProjectsRepository(db).get_by(slug=slug.upper())
    except EntityNotFoundError as e:
        raise NotFoundError(repr(e))
    project_member = ProjectMembersRepository(db).get_by(
        project_id=project.id, user_id=form_data.user_id, ignore_not_found=True)
    if not project_member:
        raise BadRequestError((
            f'User: {form_data.user_id} is not a member of project "{slug}"',
            'body.user_id'
        ))
    # find and remove permissions (should be one but it doesnt mattar)
    permission_repository = PermissionsRepository(db)
    existing_permissions = permission_repository.filter(text(
        f"user_id = {form_data.user_id} "
        f"and name = '{form_data.name}' "
        f"and CAST(data->>'project_slug' AS VARCHAR) = '{slug}' "
    ))
    if len(existing_permissions) == 0:
        raise BadRequestError((
            f'Permission {form_data.name} not exists for user: {form_data.user_id} in project {slug}',
            'body.name'
        ))
    for p in existing_permissions:
        permission_repository.delete_entity(p, autocommit=False)
    permission_repository.commit()
    return None


@router.get("/projects/{slug}/permissions", tags=['project-permissions'],
            response_model=ProjectPermissionsPaginated,
            responses=res.AUTHENTICATED | res.PROTECTED | res.NOT_FOUND,
            dependencies=[
                Depends(authenticated_user),
                Depends(Restricted([IsProjectMember]))])
async def list_project_persmisison(
        slug: str,
        page: int = 0, limit: int = 100, order_by: str = None,
        db: Session = Depends(get_db)):
    """
    List permissions paginated for project.
    """
    # todo: if query
    f = text(f"CAST(data->>'project_slug' AS VARCHAR) = '{slug.upper()}'")
    return ProjectPermissionsPaginated.from_paginated_query(
        PermissionsRepository(db).filter_paginated(f, page=page, limit=limit, order=order_by)
    )
