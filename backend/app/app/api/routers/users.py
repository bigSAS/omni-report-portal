from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from fastapi.security import OAuth2PasswordRequestForm
from datetime import timedelta
from pydantic import BaseModel, constr
from app.api.permissions.user_permissions import IsAdmin
from app.database.models.group_user import GroupUser as GroupUserEntity
from app.database.models.user import User as UserEntity
from app.api.auth import auth
from app.api.dependencies import get_db, authenticated_user, Restricted
from app.api.schemas.user import User, UserCreate, UsersPaginated, ChangePassword, UserMe
from app.errors.api import ErrorMessage, BadRequestError, NotFoundError, AuthError
from app.errors.repository import EntityNotFoundError
from app.repositories.group_users import GroupUsersRepository
from app.repositories.permissions import PermissionsRepository
from app.repositories.project_members import ProjectMembersRepository
from app.repositories.projects import ProjectsRepository
from app.repositories.users import UserRepository
from app.database.models.project import Project as ProjectEntity
# from app.services.messaging.email import send_email
# from app.config import settings

from app.database.models.permission import Permission as PermissionEntity
from app.api.routers import responses as res
from app.database.models.project_member import ProjectMember as ProjectMemberEntity

router = APIRouter()


def __get_user_data(db, user) -> dict:
    user_permissions = PermissionsRepository(db).filter(PermissionEntity.user_id == user.id)
    user_groups = GroupUsersRepository(db).filter(GroupUserEntity.user_id == user.id)
    # todo: group names with UserGroupRepositoryQuery?
    user_owned_projects = ProjectsRepository(db).filter(ProjectEntity.created_by_user_id == user.id)
    user_member_objects = ProjectMembersRepository(db).filter(ProjectMemberEntity.user_id == user.id)
    user_member_of_projects = ProjectsRepository(db).filter(
        ProjectEntity.id.in_((pm.project_id for pm in user_member_objects)))

    data = {
        'username': user.username,
        'email': user.email,
        'permissions': [{'name': permission.name, 'data': permission.data} for permission in user_permissions],
        'user_groups': [{'group_id': user_group.group_id} for user_group in user_groups],
        'user_owned_projects': [p.slug for p in user_owned_projects],
        'user_member_of_projects': [p.slug for p in user_member_of_projects]
    }
    return data


@router.post("/change-password", tags=['users'],
             status_code=204)
async def change_password(form_data: ChangePassword, db: Session = Depends(get_db)):
    """
    Change user password.
    """
    if form_data.new_password != form_data.new_password_repeat:
        raise BadRequestError(("new_password and new_password_repeat must be the same", "body.new_password"))
    user_repository = UserRepository(db)
    db_user = user_repository.get_by(email=form_data.email, ignore_not_found=True)
    if not db_user:
        raise BadRequestError((f"Email: {form_data.email} not exist", "body.email"))
    try:
        auth.authenticate_user(db=db, username=db_user.username, password=form_data.old_password)
    except AuthError:
        raise BadRequestError(("old_password invalid", "body.old_password"))
    if form_data.new_password == form_data.old_password:
        raise BadRequestError(("new_password must be different than old_password", "body.new_password"))
    # todo: password rules ect.
    new_password = auth.get_password_hash(form_data.new_password)
    db_user.hashed_password = new_password
    user_repository.save(db_user)
    return None


class AuthUser(BaseModel):
    username: constr(max_length=20)
    password: constr(max_length=30)


class TokenReponse(BaseModel):
    access_token: str
    token_type: str = "bearer"


@router.post("/token", tags=['auth'],
             responses={400: {'model': ErrorMessage}},
             response_model=TokenReponse)
async def get_jwt(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    """
    Authenticate and obtain JWT.
    """

    auth_user = AuthUser(username=form_data.username, password=form_data.password)
    user = auth.authenticate_user(db=db, username=auth_user.username, password=auth_user.password)
    data = __get_user_data(db, user)
    access_token_expires = timedelta(minutes=auth.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = auth.create_access_token(data=data, expires_delta=access_token_expires)
    return TokenReponse(access_token=access_token)


@router.post("/token/refresh", tags=['auth'],
             responses=res.AUTHENTICATED,
             response_model=TokenReponse)
async def refresh_jwt(current_user: User = Depends(authenticated_user), db: Session = Depends(get_db)):
    """
    Refresh JWT.
    """
    user = UserRepository(db).get_by(
        username=current_user.username, is_deleted=False, is_active=True, ignore_not_found=True)
    if not user: raise AuthError('Cannot refresh jwt based on user data in token. User inactive/deleted?')
    # todo: DRY - token vs token/refresh
    data = __get_user_data(db, user)
    access_token_expires = timedelta(minutes=auth.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = auth.create_access_token(data=data, expires_delta=access_token_expires)
    return TokenReponse(access_token=access_token)


@router.get("/users", tags=['admin'],
            responses=res.AUTHENTICATED,
            response_model=UsersPaginated,
            dependencies=[Depends(authenticated_user)])
async def list_users(page: int = 0, limit: int = 100, order_by: str = None, db: Session = Depends(get_db)):
    """
    List all users with pagination.
    """
    return UsersPaginated.from_paginated_query(
        UserRepository(db).all_paginated(
            page=page,
            limit=limit,
            order=order_by
        )
    )


@router.get("/users/me", tags=['users'],
            responses=res.AUTHENTICATED,
            response_model=UserMe)
async def get_request_user(current_user: User = Depends(authenticated_user), db: Session = Depends(get_db)):
    """
    Get current user based on JWT.
    """
    data = __get_user_data(db, current_user)
    data['id'] = current_user.id
    return data


@router.get("/users/{user_id}", tags=['users'],
            responses=res.AUTHENTICATED | res.NOT_FOUND,
            dependencies=[Depends(authenticated_user)],
            response_model=User)
async def get_user(user_id: int, db: Session = Depends(get_db)):
    """
    Get user by id.
    """
    return UserRepository(db).get(entity_id=user_id)


@router.post("/users", tags=['users'],
             response_model=User)
async def create_user(form_data: UserCreate, db: Session = Depends(get_db)):

    # todo: send email l8r
    # background_tasks: BackgroundTasks <- add in func signature <- from fastapi import BackgroundTasks
    # if settings.SMTP_SERVER != "your_stmp_server_here":
    #     background_tasks.add_task(send_email, user.email,
    #                               message=f"You've created your account!")
    new_user = UserEntity(
        username=form_data.username,
        email=form_data.email,
        hashed_password=auth.get_password_hash(form_data.password)
    )
    user_repository = UserRepository(db)
    existing_email = user_repository.get_by(email=form_data.email, ignore_not_found=True)
    if existing_email:
        raise BadRequestError(("Email already registered", "body.email"))
    existing_username = user_repository.get_by(username=form_data.username, ignore_not_found=True)
    if existing_username:
        raise BadRequestError(("Username already registered", "body.username"))
    user_repository.save(new_user)
    return new_user


@router.delete("/users/{user_id}", tags=['admin'],
               status_code=204,
               response_model=None,
               responses=res.AUTHENTICATED | res.PROTECTED | res.NO_CONTENT | res.NOT_FOUND,
               dependencies=[
                 Depends(authenticated_user),
                 Depends(Restricted([IsAdmin]))
               ])
def delete_user(user_id: int, db: Session = Depends(get_db)):
    try:
        UserRepository(db).delete(user_id)
    except EntityNotFoundError as e:
        raise NotFoundError(repr(e))
    return None
