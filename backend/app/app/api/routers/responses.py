from typing import List
from pydantic import BaseModel
from app.errors import ErrorMessage


class ErrorBody(BaseModel):
    messages: List[ErrorMessage]


# 4 auth protected endpoints
AUTHENTICATED = {
    400: {'model': ErrorBody},
    401: {'model': ErrorBody},
}


# 4 permission protected endpoints
PROTECTED = {
    400: {'model': ErrorBody},
    403: {'model': ErrorBody},
}

NOT_FOUND = {
    400: {'model': ErrorBody},
    404: {'model': ErrorBody},
}

# 4 no response body endpoints
NO_CONTENT = {
    400: {'model': ErrorBody},
    204: {'model': None}
}
