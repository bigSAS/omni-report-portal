"""fix FK 3

Revision ID: 56ba89657bc3
Revises: 98b2ba2c1c03
Create Date: 2021-08-27 13:30:39.609307

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '56ba89657bc3'
down_revision = '98b2ba2c1c03'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
