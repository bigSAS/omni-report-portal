from json import loads
from fastapi import FastAPI
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from fastapi.exceptions import RequestValidationError
from pydantic import ValidationError
from app.api.routers import users, permissions, user_groups, projects
from app.config import settings
from app.errors.api import ApiError, UnknownApiError

tags_metadata = [
    {
        "name": "auth",
        "description": "Authenticate operations"
    },
    {
        "name": "admin",
        "description": "Admin operations"
    },
    {
        "name": "users",
        "description": "Operations with users"
    },
    {
        "name": "projects",
        "description": "Operations with projects"
    },
    {
        "name": "project-members",
        "description": "Operations with project members"
    },
    {
        "name": "project-reports",
        "description": "Operations with project reports"
    },
    {
        "name": "project-permissions",
        "description": "Operations with project permissions"
    },
]

origins = [
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:8081",
    "http://localhost:5000",
    "http://ils-jenkins-node-qa.easypack24.net:8081",
    "https://ils-jenkins-node-qa.easypack24.net:8081",
    "http://ils-jenkins-node-qa.easypack24.net:9081",
    "https://ils-jenkins-node-qa.easypack24.net:9081",
    "http://orp.inpost.pl",
    "https://orp.inpost.pl"
    # todo: production / dev origins vs env
]

app = FastAPI(
    title=settings.APP_NAME,
    description="This is a Omni Report Portal @sas-kodzi project",
    version="1.0.5-gucci",  # todo: version from config
    openapi_tags=tags_metadata
)


async def catch_exceptions_middleware(request, call_next):
    try:
        return await call_next(request)
    except Exception as e:
        # you probably want some kind of logging here
        return JSONResponse({'message': repr(e)}, status_code=500)


app.middleware('http')(catch_exceptions_middleware)

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(users.router)
app.include_router(user_groups.router)
app.include_router(permissions.router)
app.include_router(projects.router)


# todo: log requests in error handlers -> read about fast api logging first
# todo: DRY err handlers ?

# noinspection PyUnusedLocal
@app.exception_handler(RequestValidationError)
def handle_request_validation_error(request, error: RequestValidationError):
    """
    ApiError instead of RequestValidationError.
    """
    messages = []
    errors = loads(error.json())
    print(errors)
    for error in errors:
        messages.append({
            'context': ".".join([str(i) for i in error['loc']]),
            'message': error['msg']
        })

    return JSONResponse(
        status_code=400,
        content={'messages': messages}
    )


# noinspection PyUnusedLocal,PyTypeChecker
@app.exception_handler(ValidationError)
def handle_validation_error(request, error: ValidationError):
    """
    ValidationError -> RequestValidationError.
    """
    return handle_request_validation_error(request, error)


# noinspection PyUnusedLocal
@app.exception_handler(ApiError)
def handle_api_error(request, error: ApiError):
    """
    Api error handler.
    """
    print(error)
    return JSONResponse(
        status_code=error.status_code,
        content={'messages': [e.dict() for e in error.messages]}
    )


# noinspection PyUnusedLocal
@app.exception_handler(Exception)
def handle_unknown_api_error(request, exception: Exception):
    """
    Unknown Api error handler 500 - bug :(
    """
    print('unknown err')
    error = UnknownApiError(f'{exception.__class__.__name__} - {str(exception)}')
    return JSONResponse(
        status_code=error.status_code,
        content={'messages': error.messages}
    )
