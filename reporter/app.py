import json
import requests
import os
import glob

orp_url = os.environ.get('ORP_URL', None)
if not orp_url: raise EnvironmentError('ORP_URL environment variable not set!')
username = os.environ.get('ORP_USERNAME', None)
if not orp_url: raise EnvironmentError('ORP_USERNAME environment variable not set!')
password = os.environ.get('ORP_PASSWORD', None)
if not orp_url: raise EnvironmentError('ORP_PASSWORD environment variable not set!')
slug = os.environ.get('ORP_PROJECT_SLUG', None)
if not orp_url: raise EnvironmentError('ORP_PROJECT_SLUG environment variable not set!')

report_type = os.environ.get('ORP_REPORT_TYPE', None)
if not report_type: raise EnvironmentError('ORP_REPORT_TYPE environment variable not set!')

tags = os.environ.get('ORP_TAGS', None)
if not tags: raise EnvironmentError('ORP_TAGS environment variable not set!')
tags = tags.split(",")

title = os.environ.get('ORP_TITLE', None)
if not title: raise EnvironmentError('ORP_TITLE environment variable not set!')

info = os.environ.get('ORP_INFO', None)
if not info: raise EnvironmentError('ORP_INFO environment variable not set!')

ci_system = os.environ.get('ORP_CI_SYSTEM', None)
if not ci_system: raise EnvironmentError('ORP_CI_SYSTEM environment variable not set!')

ci_job_url = os.environ.get('ORP_CI_JOB_URL', None)
if not ci_job_url: raise EnvironmentError('ORP_CI_JOB_URL environment variable not set!')

ci_job_status = os.environ.get('ORP_CI_JOB_STATUS', None)
if not ci_job_status: raise EnvironmentError('ORP_CI_JOB_STATUS environment variable not set!')

if __name__ == '__main__':
    print('ORP reporter')
    print('url', orp_url)
    token_response = requests.post(
        url=f'{orp_url}/token',
        data={
            'username': username,
            'password': password
        },
        verify=False
    )
    print('auth response:', token_response.status_code)
    if token_response.status_code != 200:
        raise ValueError(f'Authentication failed: [{token_response.status_code}]\n{token_response.json()}')

    reports_root = '/app/reports'
    report_files = glob.glob(f"{reports_root}/*.json")
    print('Report files: ', report_files)
    if len(report_files) == 0: raise EnvironmentError(f'No reports found in: {reports_root}')

    reports = []  # java cucumber reports 4 now
    for report_file in report_files:
        with open(report_file, mode='r', encoding="UTF-8") as f:
            report = json.load(f)
            reports += report
    report_data = reports
    report_meta = {
        "type": report_type,
        "tags": tags,
        "title": title,
        "info": info,
        "ci_system": ci_system,
        "ci_job_url": ci_job_url,
        "ci_job_status": ci_job_status
    }
    print('report meta:', json.dumps(report_meta, indent=2, sort_keys=True))
    project_slug = slug
    url = f'{orp_url}/projects/{project_slug}/reports'
    print('POST', url)
    body = json.dumps({'meta': report_meta, 'data': report_data})
    report_response = requests.post(
        url=url,
        data=body,
        headers={
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {token_response.json()["access_token"]}'
        },
        verify=False
    )
    print('report response status:', report_response.status_code)
    if report_response.status_code != 200:
        raise ValueError(f'Failed to upload report: [{report_response.status_code}]\n{report_response.json()}')
    print('SUCCESS!')
    print(f'report url: {orp_url}/projects/{project_slug}/reports/{report_response.json()["id"]}')
