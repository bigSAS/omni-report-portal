orpVersion=1.0
dockerRepo=bigsas

docker login -u "$DOCKER_USER" -p "$DOCKER_TOKEN" docker.io
docker build -t orp-reporter:"$orpVersion" .
docker tag orp-reporter:"$orpVersion" "$dockerRepo"/orp-reporter:"$orpVersion"
docker push "$dockerRepo"/orp-reporter:"$orpVersion"
