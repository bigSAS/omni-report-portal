```shell
docker run \
  --name ${CONTAINER} \
  -v ${params.reportsDir}:/app/reports:ro \
  -e ORP_URL=${ORP_URL} \
  -e ORP_USERNAME=${ORP_USERNAME} \
  -e ORP_PASSWORD=${ORP_PASSWORD} \
  -e ORP_PROJECT_SLUG=${ORP_PROJECT_SLUG} \
  -e ORP_REPORT_TYPE=JAVA_CUCUMBER_JSON \
  -e ORP_TAGS="${params.tags}" \
  -e ORP_TITLE="${params.title}" \
  -e ORP_INFO="${params.info}" \
  -e ORP_CI_SYSTEM=Jenkins \
  -e ORP_CI_JOB_URL="${params.jobUrl}" \
  -e ORP_CI_JOB_STATUS="${params.jobStatus}" \
  ${IMAGE}
```
