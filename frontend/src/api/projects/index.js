import { getAuthHeaders } from '../authHeaders'

export default {
  /**
   * @param {*} client axios
   * @param {*} params { page, limit, orderBy, onlyMyProjects }
   * @returns axios response
   */
  getProjects: (client, params) => {
    const page = params.page ?? 1
    const limit = params.limit ?? 100
    const orderBy = params.orderBy ?? ''
    let url = `/projects?page=${page}&limit=${limit}&order_by=${orderBy}`
    if (params.onlyMyProjects) url = `/projects/mine?page=${page}&limit=${limit}&order_by=${orderBy}` 
    return client.get(url, { headers: getAuthHeaders() })
  },
  /**
   * Create new project
   * 
   * @param {*} client axios
   * @param {*} data { name, slug, description }
   * @returns 
   */
  createProject: (client, data) => {
    return client.post(`/projects`, data, { headers: getAuthHeaders() })
  },
  /**
   * Get Project
   * 
   * @param {*} client axios
   * @param {*} slug - project slug
   * @returns 
   */
   getProject: (client, slug) => {
    return client.get(`/projects/${slug}`, { headers: getAuthHeaders() })
  },
  /**
   * Get Project Summary
   * 
   * @param {*} client axios
   * @param {*} slug - project slug
   * @returns 
   */
   getProjectSummary: (client, slug) => {
    return client.get(`/projects/${slug}/summary`, { headers: getAuthHeaders() })
  },
  /**
   * Get Project members
   * 
   * @param {*} client axios
   * @param {*} slug - project slug
   * @returns 
   */
   getProjectMembers: (client, slug) => {
    return client.get(`/projects/${slug}/members`, { headers: getAuthHeaders() })
  },
  /**
   * Get Project reports
   * 
   * @param {*} client axios
   * @param {*} slug - project slug
   * @param {*} pagination { page, limit, orderBy }
   * @param {*} filters {} // todo:
   * @returns axios response
   */
  getProjectReports: (client, slug, pagination, filters = null) => {
    const getQuery = (filters) => {
      let query = ''
      if (filters.tags && filters.tags.length > 0) query += `tags=${filters.tags.join(',')}`
      // todo: other filters
      return query
    }
    const paginate = {
      page: pagination.page ?? 1,
      limit: pagination.limit ?? 100,
      orderBy: pagination.orderBy ?? '-created_date'
    }
    let url = `/projects/${slug}/reports?page=${paginate.page}&limit=${paginate.limit}&order_by=${paginate.orderBy}`
    if (filters) {
      url += `&query=${getQuery(filters)}`
    }
    return client.get(url, { headers: getAuthHeaders() })
  },
  /**
   * Get Project report
   * 
   * @param {*} client axios
   * @param {*} slug - project slug
   * @param {*} id - report uuid
   * @returns axios response
   */
   getProjectReport: (client, slug, id) => {
    let url = `/projects/${slug}/reports/${id}`
    return client.get(url, { headers: getAuthHeaders() })
  },
  /**
   * Delete Project report
   * 
   * @param {*} client axios
   * @param {*} slug - project slug
   * @param {*} id - report uuid
   * @returns axios response
   */
   deleteProjectReport: (client, slug, id) => {
    let url = `/projects/${slug}/reports/${id}`
    return client.delete(url, { headers: getAuthHeaders() })
  }
}
