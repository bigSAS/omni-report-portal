import axios from 'axios'
import auth from './auth'
import projects from './projects'

const client = axios.create({
  baseURL: process.env.VUE_APP_BACKEND_URL,
  timeout: 30000
})


export default {
  auth: {
    /**
     * Get token request
     * 
     * @param {*} data {username, password}
     * @returns axios response
     */
    getToken: (data) => auth.getToken(client, data),
    /**
     * Refresh token request
     * 
     * @returns axios response
     */
     refreshToken: () => auth.refreshToken(client),
    /**
     * Get user data
     * 
     * @returns axios response
     */
     getUser: () => auth.getUser(client),
     /**
     * @param {*} data { username, email, password }
     * @returns axios response
     */
    register: (data) => auth.register(client, data)
  },

  projects: {
    /**
     * Get projects
     * 
     * @param {*} params { page, limit, orderBy, onlyMyProjects }
     * @returns axios response
     */
    getProjects: (params) => projects.getProjects(client, params),
    /**
     * Create new project
     * 
     * @param {*} data { name, slug, description }
     * @returns 
     */
    createProject: (data) => projects.createProject(client, data),
    /**
     * Get project
     * 
     * @param {*} slug - project slug
     * @returns 
     */
    getProject: (slug) => projects.getProject(client, slug),
     /**
     * Get project summary
     * 
     * @param {*} slug - project slug
     * @returns 
     */
    getProjectSummary: (slug) => projects.getProjectSummary(client, slug),
     /**
     * Get project members
     * 
     * @param {*} slug - project slug
     * @returns 
     */
    getProjectMembers: (slug) => projects.getProjectMembers(client, slug),
      /**
     * Get project reports
     * 
     * @param {*} slug - project slug
     * @param {*} pagination { page, limit, orderBy }
     * @param {*} filters {} // todo:
     * @returns 
     */
    getProjectReports: (slug, pagination, filters = null) => projects.getProjectReports(client, slug, pagination, filters),
     /**
     * Get project report
     * 
     * @param {*} slug - project slug
     * @param {*} id - report uuid
     * @returns 
     */
    getProjectReport: (slug, id) => projects.getProjectReport(client, slug, id),
    /**
     * Delete project report
     * 
     * @param {*} slug - project slug
     * @param {*} id - report uuid
     * @returns 
     */
    deleteProjectReport: (slug, id) => projects.deleteProjectReport(client, slug, id)
  }
}
