import store from '@/store'

const { token } = store

export const getAuthHeaders = () => {
    const headers = {}
    headers['Content-Type'] = 'application/json'
    if (token.value) headers['Authorization'] = `Bearer ${token.value}`
    return headers
  }
