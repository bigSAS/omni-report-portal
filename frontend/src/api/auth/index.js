import { getAuthHeaders } from '../authHeaders'

export default {
    /**
     * @param {*} client axios
     * @param {*} data { username, password }
     * @returns axios response
     */
    getToken: (client, data) => {
        const params = new URLSearchParams()
        params.append('username', data.username)
        params.append('password', data.password)
        return client
            .post('/token', params, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
    },

     /**
     * @param {*} client axios
     * @param {*} data { username, email, password }
     * @returns axios response
     */
      refreshToken: (client) => {
        return client
            .post('/token/refresh', {}, {headers: getAuthHeaders()})
    },

    /**
     * @param {*} client axios
     * @returns axios response
     */
     getUser: (client) => {
        return client
            .get('/users/me', {headers: getAuthHeaders()})
    },

    /**
     * @param {*} client axios
     * @param {*} data { username, email, password }
     * @returns axios response
     */
     register: (client, data) => {
        return client
            .post('/users', data)
    }
}
