import Vue from 'vue'
import VueCompositionAPI from '@vue/composition-api'

import { computed, reactive } from "@vue/composition-api"

Vue.use(VueCompositionAPI)

const DEFAULT_TIMEOUT = 5000
const snackState = reactive({
  color: 'blue',
  show: false,
  text: '...',
  timeout: DEFAULT_TIMEOUT
})
// getters
const snack = computed(() => snackState)
// setters
/**
 * Show (set) new snackbar. Overrides if another is showing.
 * 
 * @param {*} data {color, text}
 */
const showSnack = (data) => {
  console.log('show snack', data)
  snackState.show = false
  snackState.timeout = data.timeout ?? DEFAULT_TIMEOUT
  setTimeout(() => {
    snackState.color = data.color
    snackState.text = data.text
    snackState.show = true
    console.log('snack state', snackState)
  }, 100)
}


const state = {
  snack,
  showSnack
}

export default state;
