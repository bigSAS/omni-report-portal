import Vue from 'vue'
import VueCompositionAPI from '@vue/composition-api'

import { computed, reactive } from "@vue/composition-api"

Vue.use(VueCompositionAPI)

// router view scope
const _routerViewState = reactive({
  loading: false
})
// getters
const viewLoading = computed(() => _routerViewState.loading)
// setters
const _setViewLoading = (isLoading) => _routerViewState.loading = isLoading
const startViewLoading = () => _setViewLoading(true)
const stopViewLoading = () => setTimeout(() => _setViewLoading(false), 700)

// user scope
const _userState = reactive({
  user: null,
  token: null
})
// getters
const user = computed(() => _userState.user)
const token = computed(() => _userState.token)
// setters
const setUser = (user) => _userState.user = user
const setToken = (token) => _userState.token = token
  
// projects scope
const _projectsState = reactive({
  projects: null
})
// getters
const projects = computed(() => _projectsState.projects)
// setters
const setProjects = (projects) => _projectsState.projects = projects

// reports scope
// todo:



const state = {
  viewLoading,
  startViewLoading,
  stopViewLoading,

  user,
  token,
  setUser,
  setToken,

  projects,
  setProjects
}

export default state;
