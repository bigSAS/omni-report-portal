import snackbar from '../../snackbar'
import router from '../../router'
import store from '../../store'

const { showSnack } = snackbar
const { setUser, setToken } = store
/**
 * Default api error handler for FORMs
 * @param {*} error axios error
 * @param {*} formErrors reactive state from form component -> [{path, info, message}]
 */
export default function (error, formErrors) {
  return new Promise((resolve) => {
    console.warn('error code', error.code)
    formErrors.messages = []
    // network error
    if (error.message === 'Network Error') {
      showSnack({
        color: 'error', 
        text: `Network Error`,
        timeout: -1
      })
    }
    else if (error.code === 'ECONNABORTED') {
      showSnack({
        color: 'error', 
        text: `Network Timeout - please reload application`,
        timeout: -1
      })
    // api error
    } else if (error.response) {
      // 400 - other validation error (biz logic) from BE
      if (error.response.status === 400) {
        console.log("400", error)
        const messages = error.response.data.messages
        messages.forEach(message => formErrors.messages.push({
          context: message.context,
          message: message.message
        }))
      // 401 - auth error (ivalid token)
      } else if (error.response.status === 401) {
        console.log("401", error)
        console.log('auth err!!!')
        console.log('push to login and reset user')
        router.push({name: 'Login'}).then(() => {
          setUser(null)
          setToken(null)
          showSnack({
            color: 'error', 
            text: `Authentication error: ${error.response.data.messages[0].message}`,
            timeout: -1
          })
        })
        .catch(e => {
          if (e.name === 'NavigationDuplicated') {
            console.log('duplicated nav, do not redirect to Login')
          } else {
            // todo: show err msg?
            throw e
          }
        })
      // 403 - permission error
      } else if (error.response.status === 403) {
        console.error("403", error)
        router.push({name: 'Forbidden'})
      // 404 - not found
      } else if (error.response.status === 404) {
        console.error("404", error)
        router.push({name: 'NotFound'})
      } else if (error.response.status >= 500) {
        console.error("500", error)
        showSnack({
          color: 'error', 
          text: `Server ERROR - please report BUG - ${error.response.data.message}`,
          timeout: -1
        }) 
      }
     } else {
        console.error(error)
        showSnack({
          color: 'error', 
          text: `UNKNOWN ERROR - please report BUG (check devtools)`,
          timeout: -1
        })
      }
      resolve()
  })
}
