import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export const themes = {
  // todo: tweak colors ;)
  light: {
    primary: '#f5e642',
    secondary: '#cccc1f',
    accent: '#8c9eff', // ???
    success: "#aec255",
    error: '#d90016',
  },
}

export default new Vuetify({
  theme: {
    themes: themes
  }
})
