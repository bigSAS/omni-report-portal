//https://www.npmjs.com/package/dateformat
import dateFormat from 'dateformat'

export default function formatDate(datetimeString, format="dd-mm-yyyy HH:MM") {
  return dateFormat(new Date(datetimeString), format)
}
