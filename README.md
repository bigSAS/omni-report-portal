# Omni Report Portal


## How to run local backend environment

```shell
docker-compose build --build-arg APP_ENV=dev backend && \
docker-compose up -d backend
# wait for db to start
docker-compose exec -T backend alembic upgrade head && \
docker-compose exec -T backend python app/scripts/init_admin.py && \
docker-compose exec -T backend pytest && \
echo '--------------------------------------------' && \
echo 'Backend is running on http://localhost:8001' && \
echo 'Swagger http://localhost:8001/docs' && \
echo 'OpenAPI http://localhost:8001/redoc'
```  

## How to wipe out db (fresh db start when env is up)  

```shell
docker-compose down && \
docker-compose rm db && \
rm -fr db_data/* && \
docker-compose up -d backend
# wait for db to start
docker-compose exec -T backend alembic upgrade head && \
docker-compose exec -T backend python app/scripts/init_admin.py && \
docker-compose exec -T backend pytest
```

## How to generate example project (dev env is up)

```shell
docker-compose exec -T backend python app/scripts/reports/generate_example_project.py
```


## How to make db migration

```shell
docker-compose exec backend alembic revision --autogenerate -m "{message}"
docker-compose exec backend alembic upgrade head
```

## todo: front readme
